# queyras

<img src="queyras.png" width="33%"/>

---

# A minimal deep learning image classifier, implemented in the french Alps with R and Keras	

#### What is `queyras`?
`queyras` is a minimal interface to the R package `keras` which is a word-for-word interface to the `Python` library `Keras`.

#### What is `Keras`?
`Keras` is one of the leading framework for deep learning, used by hundreds of thousands of programmers. It is in `Python`, the preferred language in machine learning. It is build on another leading framework, `TensorFlow` which is in `C++` and `CUDA` (*nota bene:*  `Keras` now comes packaged with `TensorFlow 2.0`)

#### What `queyras` can do?
`queyras` can do image classification with CNNs (convolutionnal neural networks). It can  adapt and train a *pre-trained* CNN model ([ResNet50 or MobileNet](https://keras.io/api/applications/)) using annotated images. Finally, `queras` can use the trained CNN to perform class prediction on a series of new images.

#### What `queyras` can't do?
Well.. actually almost everything. In particular, it can not perform object detection or image segmentation.

#### Do I need a `GPU` card to use `queyras`?
No. You can run the code on any classical computer. However, if you run it on a `GPU` card, you can expect a x100 increase in speed.

#### Is `queyras` fast?

No. Deep learning requires a lot of computation. For more than a few hundreds of images, the computing time can last from a few minutes to a few hours (or more). Unless you have a `GPU` card... To get started, it is a good practice to freeze the weights of the pre-trained CNN model (option `freeze`) so that they are no longer trainable: it saves a lot of computing time.

#### What are the train/validation/test datasets that I see in `queyras` help?

To train a CNN, `queyras` requires a series of images that are automatically splitted into a *train dataset* (e.g. 80% of the images) and a *validation dataset* (e.g. the remaining images). The model is fitted on the train dataset using an iterative optimization algorithm. But, to prevent overfitting, this algorithm is stopped when good classification performance are achieved on the validation dataset. 

Finally, the user can use a trained CNN to perform prediction on a new dataset (not seen during the training), a *test dataset*.

<img src="trainvalidationtest.png" width="40%"/>

#### How to interpret `queyras` training output?

When running `queyras::fit`, you actually see `Keras` training output. Something like this:

<img src="epoch.png" width="70%"/>

The first two lines show the content of the *test* and *validation* datasets. 

Then, for each epoch (iteration of the optimization procedure), you get the loss and accuracy (`loss/acc`) for the test dataset,  and for the validation dataset (`val_loss/val_acc`). What you want is `acc` **AND** `val_acc` as close to 1 as possible.

---

# 1. INSTALLATION

---

Above all, you need `R`... but also `Python3` (i.e. `Python` version `3.x`).

On `Linux`:  `python --version` tells the default version. If `Python2` is the default version, verify `python3 --version`is working. If not, install `Python3`. 

On Windows: use `anaconda` for managing your `Python3` install.



**Step 1:** Install the required R packages `plyr`, `remotes` and `keras`

NB:  the R package `tensorflow` is also installed when installing `keras`

**Step 2 (the easy way, on any OS):** Install the`Python3` dependencies from `R` (yes, from `R`!):

- for `Keras`  library, simply instal the `R` package `keras`, using the code lines in the ["Examples" section here](https://keras.rstudio.com/reference/install_keras.html).

  Remember, you must use Python3 ! If Python2 is the default version, consider typing something like in `R`:
```
library(keras)
use_python("/usr/bin/python3") 
// or use_python("C:\\xxx\Python37\python.exe")`
```

- for `Pandas` library, use these `R` lines:
```
library(tensorflow)
install_tensorflow(extra_packages="pandas", envname="r-tensorflow")
# or using a conda environment (default is virtualenv)
install_tensorflow(method="conda", extra_packages="pandas", envname="r-tensorflow")
```

On Windows, only use `method = "conda"`!! Requires a fresh install of [Anaconda](https://docs.anaconda.com/anaconda/install/windows/).

If you have a `GPU` card (see after), add option `version= "gpu"`.

- for `Pillow` library (if you don't have it by default on your system), repeat the previous step with 
```
extra_packages=c("pandas","pillow")
```

<!-- Pandas as [explained here](https://rdrr.io/cran/reticulate/man/py_install.htmlpy_install) -->

**Step 2 (the hard way, on Linux):** Install the`Python3` dependencies on your system.

This is possible using your package manager (if you have super-user `root` access) such as `apt-get, pacman` or ` yaourt`. Alternatively, you can do a local install:

* `Pandas` library. See [this documentation](https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html).

  On `Linux`: `pip3 install pandas`

* `Tensorflow` framework and `Keras` library. See [this documentation](https://www.tensorflow.org/install).
  
  On `Linux`: `pip3 install tensorflow`
  
* depending on your system set-up, install `Pillow` library as well.
  On `Linux`: `pip3 install pillow`.

**Step 3:** Install `queyras` using `remotes` (or `devtools`):

```
library(remotes) # or library(devtools)
install_git("https://gitlab.com/ecostat/queyras.git")
```

**Step 4 (optional)**: If you have a `GPU` card, then install the drivers and `CUDA` (ask your sysadmin, or browse the web, or ask a friend...). 

---

# 2. EXAMPLE OF USE (with the default parameters)

---

*Warning 1:* On `Linux`, before running `R`, ask for "unlimited" amount of resources to be accessed. To do so, use the command `ulimit -s unlimited`, otherwise `R` returns the error message `C stack usage is too close to the limit`.

*Warning 2:* Remember, you must use Python3 ! If Python2 is the default version, consider typing something like:
```
library(queyras)
use_python("/usr/bin/python3") 
// or use_python("C:\\xxx\Python37\python.exe")`
```

*Warning 3:* To use the `GPU` card on `Linux`, before running `R`, specify the appropriate `CUDA` paths with something like this:
```
export PATH=/usr/local/cuda/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/cuda-10.0/extras/CUPTI/lib64
```

**Data:**

To run this example, download the "Flowers" example dataset ([zip archive](http://pbil.univ-lyon1.fr/datasets/imaginecology/Flowers.zip) or [tar/gz archive](http://pbil.univ-lyon1.fr/datasets/imaginecology/Flowers.tgz)) with two directories `Train` and `Test`, or use your preferred dataset.

**Train:** 
Consider a directory containing different classes of images, each one in a specific directory. Like this:

```
Train/Tulip
Train/Daisy
Train/Iris
```
Then run:

```
training <- queyras::train("Train")
```
The CNN model has been trained. To visualize the convergence of the estimation procedure:
```
plot(training$hist)
```
The estimated parameters were saved in a file which name is in `training$filepath`. 

**Predict:**
Consider a directory containing new images to classify. Like this:

```
Test/img1.jpg
Test/img2.jpg
```
Then predict their label with the previously trained CNN:

```
prediction <- queyras::predict("Test", training$filepath, training$labels)
```
To see the number of images predicted with each label:
```
table(prediction$pred)
```
**Retrain:**
Training was not long enough (number of epochs too small to achieve good performance)? Then re-train the model:
```
training <- queyras::train("Train", filepath=training$filepath)
```

---

# 3. CONTACT

---

For any question, bug or feedback, feel free to open an [issue](https://gitlab.com/ecostat/queyras/-/issues) (it takes 3 seconds ;-) ) :

- you have a `gitlab` account: sign-in and open an issue directly on the website;
- you don't have an account: send an email to incoming+ecostat-queyras-20659070-issue-@incoming.gitlab.com and it will become automatically a **confidential** issue (i.e. a private conversation)

Alternatively, send an email to [Vincent Miele](https://lbbe.univ-lyon1.fr/-Miele-Vincent-.html) <!--or use the Gitlab Service Desk-->

---

# 4. TROUBLESHOOTING

---

**T1:** When I run `queyras` for the first time, I have this message:
```
No non-system installation of Python could be found.
Would you like to download and install Miniconda?
Miniconda is an open source environment management system for Python.
See https://docs.conda.io/en/latest/miniconda.html for more details.
Would you like to install Miniconda? [Y/n]:
```
Do I need to install miniconda ? 
> The answer is NO. Type "n"

**T2:** I have strange warnings or a problem with `Python` environments.
> Is there any micmac with the version of `Python`? Consider using the `use_python` function...

**T3:** `R` returns the error message `C stack usage is too close to the limit`.

> On `Linux`, before running `R`, ask for "unlimited" amount of resources to be accessed. To do so, use the command `ulimit -s unlimited`.
