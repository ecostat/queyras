grepl("\\.h5", filepath, fixed = FALSE)


## load model
model <- load_model_hdf5(filepath)

if (freeze) freeze_weights(model[[1]])

fileprefix <- strsplit(filepath, ".h5")[[1]]
h5filepath <- paste(fileprefix,train_date,"_retrained.h5",sep="")
