## https://www.shirin-glander.de/2018/06/keras_fruits/

library(keras)
warning("On Linux, required ulimit -s unlimited")

## list of fruits to modle
fruit_list <- c("Kiwi", "Banana", "Apricot")

## number of output classes (i.e. fruits)
output_n <- length(fruit_list)

## image size to scale down to (original images are 100 x 100 px)
img_width <- 224
img_height <- 224
target_size <- c(img_width, img_height)

## define batch size and number of epochs
batch_size <- 32
epochs <- 10

## RGB = 3 channels
channels <- 3

## path to image folders
train_image_files_path <- "fruits/train/"
valid_image_files_path <- "fruits/val"
imagedir <- "fruits/train/"

if(FALSE){
## optional data augmentation
train_data_gen = image_data_generator(
  rescale = 1/255 ##,
  ##rotation_range = 40,
  ##width_shift_range = 0.2,
  ##height_shift_range = 0.2,
  ##shear_range = 0.2,
  ##zoom_range = 0.2,
  ##horizontal_flip = TRUE,
  ##fill_mode = "nearest"
)

## Validation data shouldn't be augmented! But it should also be scaled.
valid_data_gen <- image_data_generator(
  rescale = 1/255
)  

## training images
train_image_array_gen <- flow_images_from_directory(train_image_files_path, 
                                          train_data_gen,
                                          target_size = target_size,
                                          class_mode = "categorical",
                                          classes = fruit_list,
                                          seed = 42)

## validation images
valid_image_array_gen <- flow_images_from_directory(valid_image_files_path, 
                                          valid_data_gen,
                                          target_size = target_size,
                                          class_mode = "categorical",
                                          classes = fruit_list,
                                          seed = 42)
} else{  
    #### processing the data path and deducing the labels
    classlabels <- list.files(imagedir)
    nbclasses <- length(classlabels)
    l <- lapply(classlabels, function(classlabel){
        filenames <- list.files(paste(imagedir,classlabel,sep="/"))
        return(cbind(filename=paste(classlabel,filenames,sep="/"), label=rep(classlabel,length(filenames))))
    })
    library (plyr)
    df <- ldply (l, data.frame)
    
    #### make balanced dataframe
    #### TODO    
    #### shuffling, since the 20% validation set is the 20% last images
    df <- df[sample(nrow(df)),]#### data generators (Python Pandas must be installed)
    
    datagen <- image_data_generator(preprocessing_function = imagenet_preprocess_input)

train_image_array_gen <- flow_images_from_dataframe(df[1:as.integer(nrow(df)*0.8-1),],
                                        directory = imagedir,
                                        x_col = "filename",
                                        y_col = "label",
                                        class_mode = "categorical",
                                        generator = datagen,
                                        target_size = c(img_width, img_height),
                                        batch_size = batch_size,
                                        shuffle = TRUE)


valid_image_array_gen <- flow_images_from_dataframe(df[as.integer(nrow(df)*0.8):nrow(df),],
                                      directory = imagedir,
                                      x_col = "filename",
                                      y_col = "label",
                                      class_mode = "categorical",
                                      generator = datagen,
                                      target_size = c(img_width, img_height),
                                      batch_size = batch_size,
                                      shuffle = TRUE)
}



cat("Number of images per class:")

#### Number of images per class:

table(factor(train_image_array_gen$classes))


cat("\nClass label vs index mapping:\n")
train_image_array_gen$class_indices
fruits_classes_indices <- train_image_array_gen$class_indices


## number of training samples
train_samples <- train_image_array_gen$n
## number of validation samples
valid_samples <- valid_image_array_gen$n


cnn_base <- application_mobilenet(weights = "imagenet", include_top = FALSE, pooling = "avg", input_shape = c(img_width, img_height, channels))

if(FALSE)
    cnn_base <- application_resnet50(weights = "imagenet", include_top=FALSE, input_shape = c(img_width, img_height, channels))
freeze_weights(cnn_base)

model <- keras_model_sequential() %>%
   cnn_base() %>%
  ## Flatten max filtered output into feature vector 
  ## and feed into dense layer
  layer_flatten() %>%
  layer_dense(128) %>%
  layer_activation("relu") %>%
  layer_dropout(0.5) %>%

  ## Outputs from dense layer are projected onto output layer
  layer_dense(output_n) %>% 
  layer_activation("softmax")

if(FALSE){
## initialise model
model <- keras_model_sequential()
## add layers
model %>%
  layer_conv_2d(filter = 32, kernel_size = c(3,3), padding = "same", input_shape = c(img_width, img_height, channels)) %>%
  layer_activation("relu") %>%
  
  ## Second hidden layer
  layer_conv_2d(filter = 16, kernel_size = c(3,3), padding = "same") %>%
  layer_activation_leaky_relu(0.5) %>%
  layer_batch_normalization() %>%

  ## Use max pooling
  layer_max_pooling_2d(pool_size = c(2,2)) %>%
  layer_dropout(0.25) %>%
  
  ## Flatten max filtered output into feature vector 
  ## and feed into dense layer
  layer_flatten() %>%
  layer_dense(100) %>%
  layer_activation("relu") %>%
  layer_dropout(0.5) %>%

  ## Outputs from dense layer are projected onto output layer
  layer_dense(output_n) %>% 
  layer_activation("softmax")
}

## compile
model %>% compile(
  loss = "categorical_crossentropy",
  optimizer = optimizer_rmsprop(lr = 0.0001, decay = 1e-6),
  metrics = "accuracy"
  )

## fit
hist <- model %>% fit_generator(
  ## training data
  train_image_array_gen,
  
  ## epochs
  steps_per_epoch = ceiling(train_samples / batch_size), 
  epochs = epochs, 
  
  ## validation data
  validation_data = valid_image_array_gen,
  validation_steps = ceiling(valid_samples / batch_size),
  
  ## print progress
  verbose = 2,
  callbacks = list(
    callback_model_checkpoint(
        ## save best model for val_loss criteria
        filepath = "resnet_{epoch:02d}_{val_loss:.2f}h.h5",
        monitor = "val_accuracy",
        #monitor = "val_loss",
        save_best_only = FALSE,
        save_weights_only = FALSE,
        mode = "min"
    ),
    callback_early_stopping(
        monitor = "val_loss",
        min_delta = 1e-4, # absolute change of less than min_delta, will count as no improvement
        patience = 3, # number of epochs with no improvement after which training will be stopped
        verbose = 1,
        mode = "min"
    ),
    callback_reduce_lr_on_plateau(
        monitor = "val_loss",
        factor = 0.5,
        patience = 4,
        verbose = 1,
        min_delta = 1e-4,
        mode = "min"
    )
  )
)

## make predictions then decode and print them
## flow_images_from_directory expects a subdirectory : we don't use it here
testimagedir <- "flowerstest"
df <- data.frame(filename=list.files(testimagedir))
test_gen <- flow_images_from_dataframe(df,
                                       directory = testimagedir,
                                       x_col = "filename",
                                       class_mode = NULL,
                                       generator = image_data_generator(),
                                       target_size = c(img_width, img_height),
                                       batch_size = batch_size,
                                       shuffle = FALSE)
test_samples <- test_gen$n
model <- load_model_hdf5("resnet_02_0.18h.h5")
preds <- model %>% predict_generator(generator=test_gen,
                                     steps=ceiling(test_samples / batch_size))

data.frame(filename=test_gen$filenames, class=which.max(preds))
