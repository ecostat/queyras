\name{queyras-package}
\alias{queyras-package}
\alias{queyras}
\docType{package}
\title{
A minimal deep learning image classifier
}
\description{
A minimal deep learning image classifier, implemented in the french Alps with R and Keras
}
\details{
queyras is a minimal interface to the R package keras which is a word-for-word interface to the Python library Keras.
}
\author{
Authors: Vincent Miele
Maintainer: Vincent Miele <vincent.miele@univ-lyon1.fr>
}
\references{
}
\keyword{package}
\keyword{ecology}
\keyword{computer vision}
\keyword{deep learning}
\seealso{
  \code{\link[keras]{keras}}
}
