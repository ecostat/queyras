train <- function(directory, base="resnet", epochs=10, validation_split=0.2, validation_pred=FALSE, freeze=TRUE, seed=NULL, keep_date=FALSE, filepath=NULL){
    debug = FALSE
    ## image size to scale down to
    img_width <- 224
    img_height <- 224
    target_size <- c(img_width, img_height)
    ## RGB = 3 channels
    channels <- 3
     ## define batch size
    batch_size <- 32
    
    ## retrieve labels and filenames & building the dataframe
    if (!dir.exists(directory))
        stop("directory not found", paste("", directory))        
    labels <- list.files(directory)
    l <- lapply(labels, function(label){
        filenames <- list.files(paste(directory,label,sep="/"))
        return(cbind(filename=paste(label,filenames,sep="/"), label=rep(label,length(filenames))))
    })
    df <- ldply (l, data.frame)    
    ## shuffle the dataframe, since the 20% validation set is the 20% last images
    if (!is.null(seed)) set.seed(seed)
    df <- df[sample(nrow(df)),]

    ## creating generators from the dataframe
    datagen <- image_data_generator(preprocessing_function = imagenet_preprocess_input)
    train_gen <- flow_images_from_dataframe(df[1:as.integer(nrow(df)*(1-validation_split)-1),],
                                            directory = directory,
                                            x_col = "filename",
                                            y_col = "label",
                                            class_mode = "categorical",
                                            generator = datagen,
                                            target_size = c(img_width, img_height),
                                            batch_size = batch_size,
                                            shuffle = TRUE)
    val_gen <- flow_images_from_dataframe(df[as.integer(nrow(df)*(1-validation_split)):nrow(df),],
                                          directory = directory, classes=labels,
                                          x_col = "filename",
                                          y_col = "label",
                                          class_mode = "categorical",
                                          generator = datagen,
                                          target_size = c(img_width, img_height),
                                          batch_size = batch_size,
                                          shuffle = FALSE) # no shuffle, for further match between predictions and filenames
    if(debug){
        cat("Number of images per class:")
        print(table(factor(train_gen$classes)))
        
        cat("\nClass label vs index mapping:\n")
        print(train_gen$class_indices)
    }
    n_class <- length(train_gen$class_indices)

    if(!is.null(filepath)) base=NULL
    if(is.null(base)){
        ## load a pre-trained model = retrain
        model <- load_model_hdf5(filepath)
        if (freeze) freeze_weights(model$layers[[1]])
    } else{
        ## load a pre-trained base CNN backbone
        i_base <- pmatch(base, c("resnet","mobilenet"))
        if (is.na(i_base))
            stop("invalid pre-trained model", paste("", base))
        if(i_base==1){
            base <- "resnet"
            cnn_base <- application_resnet50(weights = "imagenet", include_top = FALSE, pooling = "avg", input_shape = c(img_width, img_height, channels))
        }
        if(i_base==2){
            base <- "mobilenet"
            cnn_base <- application_mobilenet_v2(weights = "imagenet", include_top = FALSE, pooling = "avg", input_shape = c(img_width, img_height, channels))
        }
        if (freeze) freeze_weights(cnn_base)
        ## build the classifier
        model <- keras_model_sequential() %>%
            cnn_base() %>%
            ## Flatten max filtered output into feature vector 
            ## and feed into dense layer
            layer_flatten() %>%
            #layer_dense(128) %>%
            #layer_activation("relu") %>%
            #layer_dropout(0.5) %>%        
            ## Outputs from dense layer are projected onto output layer
            layer_dense(n_class) %>% 
            layer_activation("softmax")
    }
    
    ## compile
    model %>% compile(
                  loss = "categorical_crossentropy",
                  optimizer = optimizer_rmsprop(lr = 0.0001, decay = 1e-6),
                  metrics = "acc"
                  ## According to the 2.3.0 Release Notes: "Metrics and losses
                  ## are now reported under the exact name specified by the user (e.g. "acc" or "accuracy")
              )
    ## build HDF5 filename
    if(keep_date){
        train_date <- paste("_",gsub(" ","_",gsub("  "," ",date())),sep="")
    } else train_date <- ""
    if(is.null(filepath)){
        h5filepath <- paste("queyras_",base,train_date,".h5",sep="")
    } else{
        fileprefix <- strsplit(filepath,".h5")[[1]]
        h5filepath <- paste(fileprefix,train_date,"_retrained.h5",sep="")
    }
    ## fit
    train_samples <- train_gen$n # number of training samples
    val_samples <- val_gen$n # number of validation samples
    hist <- model %>% fit_generator(
                          ## training data
                          train_gen,  
                          ## epochs
                          steps_per_epoch = ceiling(train_samples/batch_size), 
                          epochs = epochs,                           
                          ## validation data
                          validation_data = val_gen,
                          validation_steps = ceiling(val_samples/batch_size),  
                          ## print progress
                          verbose = 2,
                          ## callbacks
                          callbacks = list(
                              callback_model_checkpoint(
                                  ## save best model for criteria
                                  ##filepath = paste(base,"_{epoch:02d}_{val_loss:.2f}h.h5",sep=""),
                                  filepath = h5filepath,
                                  monitor = "val_acc", 
                                  ##monitor = "val_loss",
                                  save_best_only = TRUE,
                                  save_weights_only = FALSE,
                                  mode = "max"
                              ),
                              callback_early_stopping(
                                  monitor = "val_acc",
                                  min_delta = 1e-4, # absolute change of less than min_delta, will count as no improvement
                                  patience = 3, # number of epochs with no improvement after which training will be stopped
                                  verbose = 1,
                                  mode = "max"
                              ),
                              callback_reduce_lr_on_plateau(
                                  monitor = "val_loss",
                                  factor = 0.5,
                                  patience = 4,
                                  verbose = 1,
                                  min_delta = 1e-4,
                                  mode = "min"
                              )
                          )
                      )
    cat("Saving best model in",h5filepath,"\n")

    ## modify hist object with early stopping
    hist$params$epochs <- length(hist$metrics$loss)

    ## predict on validation set
    if(validation_pred){
        model <- load_model_hdf5(h5filepath)
        pred_score<- model %>% predict_generator(generator=val_gen,
                                                 steps=ceiling(val_samples/batch_size))
        return(list(hist=hist,                    
                    filepath=h5filepath,
                    labels=labels,
                    validation=data.frame(filename=val_gen$filenames,
                                          label=sapply(strsplit(val_gen$filenames,"/"), function(strv) strv[1]),
                                          pred=labels[apply(pred_score,1,which.max)],
                                          score=as.integer(apply(pred_score,1,max)*1000)/1000.)))
    } else{
        list(hist=hist,                  
             filepath=h5filepath,
             labels=labels,
             validation=NULL)
    }
}

